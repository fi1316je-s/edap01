using StaticArrays, HTTP, JSON3

const ROWS = 6
const COLS = 7
const NUM_CONNECT = 4

const Player = Int8

const YELLOW = Player(1)
const RED = Player(-1)

const Board = SMatrix{ROWS,COLS,Int8}

mutable struct Connect4
    board::Board
    currentplayer::Player
    winner::Player
end

Connect4() = Connect4(@SMatrix(zeros(Int8, ROWS, COLS)), YELLOW, Int8(0))

Base.copy(g::Connect4) = Connect4(g.board, g.currentplayer, g.winner)

nextplayer(p::Player) = Player(-p)

function firstfree(board::Board, col::Int)
    for (i, v) in enumerate(@view(board[:, col]))
        v != 0 && return i - 1
    end
    return ROWS
end


function isconnectedcol(board::Board)
    for col in eachcol(board), i in 1:ROWS - NUM_CONNECT + 1
        abs(sum(@view(col[i:i + NUM_CONNECT - 1]))) == NUM_CONNECT && return true
    end
    return false
end

function isconnectedrow(board::Board)
    for row in eachrow(board), i in 1:COLS - NUM_CONNECT + 1
        abs(sum(@view(row[i:i + NUM_CONNECT - 1]))) == NUM_CONNECT && return true
    end
    return false
end

function isconnecteddiag(board::Board)
    for i in 1:ROWS - NUM_CONNECT + 1, j in 1:COLS - NUM_CONNECT + 1
        abs(sum(board[i + k,j + k] for k in 0:NUM_CONNECT - 1)) == NUM_CONNECT && return true
    end
    return false
end

function isconnectedrevdiag(board::Board)
    for i in NUM_CONNECT:ROWS, j in 1:COLS - NUM_CONNECT + 1
        abs(sum(board[i - k,j + k] for k in 0:NUM_CONNECT - 1)) == NUM_CONNECT && return true
    end
    return false
end

iswin(g::Connect4) = isconnectedcol(g.board) || isconnectedrow(g.board) ||
                     isconnecteddiag(g.board) || isconnectedrevdiag(g.board)

function play!(g::Connect4, col::Int)
    row::Int = firstfree(g.board, col)
    g.board = setindex(g.board, g.currentplayer, row, col)
    if iswin(g)
        g.winner = g.currentplayer
    end
    g.currentplayer = nextplayer(g.currentplayer)
    return g
end

const BESTORDER = (4,3,5,2,6,1,7)

availablemoves(g::Connect4) = (i for i in BESTORDER if g.board[1,i] == 0)

children(g::Connect4) = (play!(copy(g), col) for col in availablemoves(g))


function colheuristics(g::Connect4, us::Player)::Int
    op, count = nextplayer(us), 0
    for j in 1:COLS, k in 1:ROWS - NUM_CONNECT + 1
        count += !any(g.board[i, j] == op for i in k:k + NUM_CONNECT - 1) 
    end
    return count
end

function rowheuristics(g::Connect4, us::Player)::Int
    op, count = nextplayer(us), 0
    for i in 1:ROWS, k in 1:COLS - NUM_CONNECT + 1
        count += !any(g.board[i, j] == op for j in k:k + NUM_CONNECT - 1)
    end
    return count
end

function diagheuristics(g::Connect4, us::Player)::Int
    op, count = nextplayer(us), 0
    for i in 1:ROWS - NUM_CONNECT + 1, j in 1:COLS - NUM_CONNECT + 1
        count += !any(g.board[i + k,j + k] == op for k in 0:NUM_CONNECT - 1)
    end
    return count
end

function revdiagheuristics(g::Connect4, us::Player)::Int
    op, count = nextplayer(us), 0
    for i in NUM_CONNECT:ROWS, j in 1:COLS - NUM_CONNECT + 1
        count += !any(g.board[i - k,j + k] == op for k in 0:NUM_CONNECT - 1)
    end
    return count
end


function heuristics(g::Connect4, us::Player)::Int
    op = nextplayer(us)
    our = colheuristics(g, us) + rowheuristics(g, us) + 
          diagheuristics(g, us) + revdiagheuristics(g, us)
    their = colheuristics(g, op) + rowheuristics(g, op) + 
            diagheuristics(g, op) + revdiagheuristics(g, op)
    return our - their
end

function minimax(g::Connect4, depth::Int, α::Int=typemin(Int), β::Int=typemax(Int))
    g.winner != 0 && return (g.winner == YELLOW ? 100 : -100) * depth
    depth == 0 && return heuristics(g, YELLOW)
    if g.currentplayer == YELLOW
        maxeval = typemin(Int)
        for child in children(g)
            eval = minimax(child, depth - 1, α, β)
            maxeval = max(maxeval, eval)
            α = max(α, eval)
            β <= α && break
        end
        return maxeval
    else
        mineval = typemax(Int)
        for child in children(g)
            eval = minimax(child, depth - 1, α, β)
            mineval = min(mineval, eval)
            β = min(β, eval)
            β <= α && break
        end
        return mineval
    end
end

function search(g::Connect4, depth::Int)
    moves = collect(availablemoves(g))
    n = length(moves)
    scores = Vector{Tuple{Int,Int}}(undef, n)
    Threads.@threads for i in 1:n
        col = moves[i]
        scores[i] = (col, g.currentplayer * minimax(play!(copy(g), col), depth - 1))
    end
    return scores
end

function findmove(g::Connect4)
    n = sum(g.board .!= 0)
    d = 10 + n ÷ 7
    println("d=", d)
    res = search(g, d)
    bestval = maximum(p[2] for p in res)
    bestmoves = [p for p in res if p[2] == bestval]
    return rand(bestmoves)
end

function playbest!(g::Connect4)
    col, val = findmove(g)
    play!(g, col)
    return col
end

function messagebot(move::Int=-1)
    server = "https://vilde.cs.lth.se/edap01-4inarow/move"
    data = HTTP.Form(Dict(
        "stil_id" => "fi1316je-s",
        "move" => string(move),
        "api_key" => "nyckel"))
    res = HTTP.post(server, [], data)
    json = JSON3.read(String(res.body))
    return json
end

botmove(response) = response.botmove + 1

function printinfo(g::Connect4, move::Int, botmove::Bool)
    player = botmove ? "Bot" : "Filip"
    println("$player played $move")
    display(g.board)
end

isterminated(g::Connect4) = g.winner != 0 || isempty(availablemoves(g))

function challangebot()
    res = messagebot()
    display(res)
    move = botmove(res)
    g = Connect4()
    if move != 0
        play!(g, move)
        printinfo(g, move, true)
    end
    while !isterminated(g)
        move = playbest!(g)
        printinfo(g, move, false)
        res = messagebot(move - 1)
        if isterminated(g)
            break
        end
        move = botmove(res)
        play!(g, move)
        printinfo(g, move, true)
    end
    println("Game over!")
    display(res)
    return res.result
end

function play20games()
    wins = 0
    losses = 0
    draws = 0
    for _ in 1:20
        res = challangebot()
        if res == 1
            wins += 1
        elseif res == -1
            losses += 1
        else
            draws += 1
        end
    end
    println("Wins=$wins, draws=$draws, losses=$losses")
end
